#! /usr/bin/env python

import math

from psfile import EPSFile




str = "ABCD"




dpi = 72

wheel_diameter = 50
hub_diameter = 2.5
width = 2
num_of_spokes = 16
spoke_dia = 0.625



wheel_cir = wheel_diameter*math.pi
wheel_spoke_spacing = wheel_cir/num_of_spokes
wheel_dpi_spoke_spacing = wheel_spoke_spacing*dpi
wheel_dpi_cir = wheel_cir*dpi

dpi_spoke_dia = math.ceil(spoke_dia*dpi)



hub_cir = hub_diameter*math.pi
hub_spoke_spacing = hub_cir/num_of_spokes
hub_dpi_spoke_spacing = hub_spoke_spacing*dpi
hub_dpi_cir = hub_cir*dpi

num_of_hub_sets = 4
hub_set_spacing = wheel_dpi_cir/num_of_hub_sets


dpi_width = width*dpi
 
image_height = math.ceil(dpi_width * 1.1 * 8 )
image_width = math.ceil(wheel_dpi_cir * 1.1)

x_ofs = dpi_width

fd = EPSFile("wheel_pattern_72dpi.eps", image_width, image_height)

fd.append("/Times-Roman findfont")

fd.append("0.5 setgray")
fd.append("""%f %f moveto"""%( 100 , image_height - 50 ))
fd.append("/Times-Roman 50 selectfont")
fd.append("(Wheel Inner Diameter %.3f in.) show" %( wheel_diameter ) )
fd.append("""closepath""")
fd.append("""stroke""")            

fd.append("""%f %f moveto"""%( 100, image_height - 110 ))
fd.append("/Times-Roman 50 selectfont")
fd.append("(Hub Outer Diameter %.3f in.) show" %( hub_diameter ) )
fd.append("""closepath""")
fd.append("""stroke""")            


fd.append("""%f %f moveto"""%( image_width/2 , image_height - 110 ))
fd.append("/Times-Roman 100 selectfont")
fd.append("(Print at 72dpi) show" )
fd.append("""closepath""")
fd.append("""stroke""")            

for y in range(0,2,1):
    y_ofs = y * dpi_width * 2
    fd.append("0.8 setgray")
    fd.append("%f %f %d %d rectfill"%(x_ofs, y_ofs, wheel_dpi_cir, dpi_width))
    for x in range(0, num_of_spokes):

        fd.append("0 setgray")
        fd.append("%f %f %f 0 360 arc"
                  %((x*wheel_dpi_spoke_spacing)+wheel_dpi_spoke_spacing*0.5+x_ofs, dpi_width/2+y_ofs, dpi_spoke_dia/2))
        fd.append("stroke")
        fd.append("""%f %f moveto"""%((x*wheel_dpi_spoke_spacing)+wheel_dpi_spoke_spacing*0.5+dpi_spoke_dia/2+x_ofs,dpi_width/2+y_ofs))
        fd.append("""%f %f rlineto"""%(0-dpi_spoke_dia,0))
        fd.append("""%f %f rlineto"""%(dpi_spoke_dia/2,dpi_spoke_dia/2))
        fd.append("""%f %f rlineto"""%(0,0-dpi_spoke_dia))
        fd.append("""closepath""")
        fd.append("""stroke""")

        
        xf = 0
        for x in range(0, int(wheel_dpi_cir + dpi) , int(dpi/4)):
            # Print tick marks
            fd.append("0.6 setgray")
            fd.append("""%f %f moveto"""%(x+x_ofs,dpi_width + y_ofs ))
            fd.append("""%f %f rlineto"""%(0, 0 - (dpi_width/4)  ))
            fd.append("""closepath""")
            fd.append("""stroke""")
            # Print the tick numbers
            fd.append("0 setgray")
            fd.append("""%f %f moveto"""%( x+x_ofs+10 , dpi_width + (dpi/2) + y_ofs ))
            fd.append("/Times-Roman 20 selectfont")
            fd.append("gsave")
            fd.append("90 rotate")
            fd.append("(%.3f) show" %( float(xf) / float(dpi) ) )
            fd.append("grestore")
            fd.append("""closepath""")
            fd.append("""stroke""")            
            xf = xf + dpi/4 





for h in range (0,num_of_hub_sets,1):
    hub_spoke_stagger = spoke_dia * (h+1)/2
    dpi_spoke_stagger = math.ceil(hub_spoke_stagger*dpi)
    hx_ofs = h*hub_set_spacing
    for y in range(2,4,1):
        y_ofs = y * dpi_width * 2
        fd.append("0.8 setgray")
        fd.append("%f %f %d %d rectfill"%(x_ofs + hx_ofs, y_ofs, hub_dpi_cir, dpi_width))
        for x in range(0, num_of_spokes):
            fd.append("0 setgray")
            fd.append("""%f %f %f 0 360 arc
                     stroke"""%((x*hub_dpi_spoke_spacing)+hub_dpi_spoke_spacing*0.5 + (x_ofs+hx_ofs), dpi_width/2+y_ofs - dpi_spoke_stagger/2 + (dpi_spoke_stagger*(x%2)) , dpi_spoke_dia/2))
            fd.append("""%f %f moveto"""%( (x*hub_dpi_spoke_spacing) + hub_dpi_spoke_spacing*0.5 + dpi_spoke_dia/2+(x_ofs+hx_ofs),
                                           dpi_width/2+y_ofs - dpi_spoke_stagger/2 + (dpi_spoke_stagger*(x%2)) ))
            fd.append("""%f %f rlineto"""%(0-dpi_spoke_dia,0))
            fd.append("""%f %f rlineto"""%(dpi_spoke_dia/2,dpi_spoke_dia/2))
            fd.append("""%f %f rlineto"""%(0,0-dpi_spoke_dia))
            fd.append("""closepath""")
            fd.append("""stroke""")

            xf = 0
            for x in range(0, int(hub_dpi_cir + dpi) , int(dpi/4)):
                # Print tick marks
                fd.append("0.6 setgray")
                fd.append("""%f %f moveto"""%(x+(x_ofs+hx_ofs),dpi_width + y_ofs ))
                fd.append("""%f %f rlineto"""%(0, 0 - (dpi_width/4)  ))
                fd.append("""closepath""")
                fd.append("""stroke""")
                # Print the tick numbers
                fd.append("0 setgray")
                fd.append("""%f %f moveto"""%( x+(x_ofs+hx_ofs)+10 , dpi_width + (dpi/2) + y_ofs ))
                fd.append("/Times-Roman 20 selectfont")
                fd.append("gsave")
                fd.append("90 rotate")
                fd.append("(%.3f) show" %( float(xf) / float(dpi) ) )
                fd.append("grestore")
                fd.append("""closepath""")
                fd.append("""stroke""")            
                xf = xf + dpi/4 


            
              
fd.close()

